import React, {useState, useEffect} from 'react';

import './App.css';

const staticDate = new Date();
const staticHours = staticDate.getHours()*3600000;
const staticMinutes = staticDate.getMinutes()*60000;

const App = () => {
  const [date, setDate] = useState(new Date(staticDate.getTime() - staticHours - staticMinutes));
  const [hours, setHours] = useState(staticHours);
  const [minutes, setMinutes] = useState(staticMinutes);
  const [currentDate, setCurrentDate] = useState(new Date());

  useEffect(() => {
    let time = setInterval(() => {
      setCurrentDate(new Date());
    }, 1000);

    return () => {
      clearInterval(time);
    };
  }, [currentDate, hours])

  const displayDate = () => {
    const normalDate = new Date(currentDate.getTime() - staticDate.getTime() + (date.getTime() + hours + minutes));

    let year = normalDate.getFullYear();
    let month = getStringMonth(normalDate.getMonth());
    let day = getStringDay(normalDate.getDay());
    let dat = normalDate.getDate();

    return {day, month, year, date:dat};
  };

  const displayTime = () => {
    const normalDate = new Date(currentDate.getTime() - staticDate.getTime() + (date.getTime() + hours + minutes));

    let h = normalDate.getHours();
    let m = normalDate.getMinutes();
    let seconds = normalDate.getSeconds();

    return {hours:h, minutes:m, seconds};
  };

  const reset = () => {
    const cDate = currentDate;
    const hours = cDate.getHours()*360000;
    const minutes = cDate.getMinutes()*60000;

    setHours(hours);
    setMinutes(minutes);
    setDate(new Date(cDate.getTime() - hours -minutes));
  };

  const getStringMonth = (month) => {
    switch (month) {
      case 0:
        return "Janvier";
      case 1:
        return "Fevrier";
      case 2:
        return "Mars";
      case 3:
        return "Avril";
      case 4:
        return "Mai";
      case 5:
        return "Juin";
      case 6:
        return "Juillet";
      case 7:
        return "Aout";
      case 8:
        return "Septembre";
      case 9:
        return "Octobre";
      case 10:
        return "Novembre";
      case 11:
        return "Decembre";
      default:
        // nothing
        break;
    }
  }

  const getStringDay = (day) => {
    switch (day) {
      case 0:
        return "Dimanche";
      case 1:
        return "Lundi";
      case 2:
        return "Mardi";
      case 3:
        return "Mercredi";
      case 4:
        return "Jeudi";
      case 5:
        return "Vendredi";
      case 6:
        return "Samedi";
      default:
        // nothing
        break;
    }
  }

  const getPeriod = () => {
    let h = hours/360000;
    if(h < 12){
      return "MATIN";
    }else if(h === 12){
      return "MIDI";
    }else if(h > 12 && h < 15){
      return "APRÈs MIDI";
    }

    return "SOIR";
  };

  let h = displayTime().hours < 10 ? "0" + displayTime().hours : displayTime().hours;
  let m = displayTime().minutes < 10 ? "0" + displayTime().minutes : displayTime().minutes;
  let s = displayTime().seconds < 10 ? "0" + displayTime().seconds : displayTime().seconds;

  return (
    <section className="container">
      <div className="display-container">
        <span className="display-elt">{displayDate().day}</span>
        <span className="display-elt">{getPeriod()}</span>
        <span className="display-elt">{`${h} : ${m} : ${s}`}</span>
        <span className="display-elt">{`${displayDate().date} ${displayDate().month} ${displayDate().year}`}</span>
      </div>

      <Setings
        hours={hours}
        minutes={minutes}
        date={date}
        setHours={setHours}
        setMinutes={setMinutes}
        setDate={setDate}
      />
    </section>
  );
};

const Setings = ({hours, minutes, date, setHours, setMinutes, setDate}) => {

  const [h, setH] = useState(hours);
  const [m, setM] = useState(minutes);
  const [d, setD] = useState(date);
  const [dTime, setDtime] = useState(date.getTime());

  useEffect(() => {
    setH(hours);
    setM(minutes);
    setD(date);
  }, [hours, minutes, date])

  const changeDate = (value) => {
    const dateTime = new Date(new Date(value).getTime - 360000);
    setD(value);
    setDtime(dateTime);
  };

  const handleChangeDate = () => {
    setMinutes(m);
    setHours(h);
    setDate(new Date(dTime));
  };

  return(
    <div className="setings-container">
        <div className="top">
          <div className="hours-box c-item">
            <label htmlFor="hours">Heure</label>
            <select id="hours" value={h} onChange={event => setH(event.target.value)}>
              {
                Array(24).fill(1).map((val, index) => <option value={index*3600000} key={index}>{`${index < 10 ? "0"+ index : index}h`}</option>)
              }
            </select>
          </div>
          <div className="minute-box c-item">
            <label htmlFor="minute">Minute</label>
            <select id="minutes" value={m} onChange={event => setM(event.target.value)}>
              {
                Array(60).fill(1).map((val, index) => <option value={index*60000} key={index}>{`${index < 10 ? '0'+index : index}m`}</option>)
              }
            </select>
          </div>
        </div>
        
        <div className="bottom">
          <div className="date-box c-item">
              <label htmlFor="hours">Date</label>
              <input type="date" id="date" value={d} onChange = {event => changeDate(event.target.value)} />
          </div>

          <div className="submit-box c-item">
            <input type="submit" id="submit" value="Modifier" onClick={()=>handleChangeDate()} />
          </div>
        </div>
    </div>
  );
};

export default App;